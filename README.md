# demo-iac-iis-server

## Env variable

The following env variable are configured for the CI/CD pipeline.
Look for the following into tasks and vars files: ```"{{ lookup('env','ENV_VARIABLE_NAME') }}"``` if you need to change the name.

### Machine key env

* decryptionKey -> IIS_DECRYPTION_KEY
* validationKey -> IIS_VALIDATION_KEY

### IIS shared config env

* shared_config_key -> IIS_SHARED_CONFIG_KEY

### IIS shared certificat env

* shared_certificat_user -> WS_ANSIBLE_USER_PWD
* shared_certificat_user_password -> WS_ANSIBLE_USER
* shared_certificat_certificate_Private_key -> IIS_SHARED_CERTIFICATE_PRIVATE_KEY

### Splunk config env

* splunk_agent_user -> SPLUNK_ACCOUNT_USER
* splunk_agent_password -> SPLUNK_ACCOUNT_PASSWORD
* splunk_credential_user -> SPLUNK_CREDENTIAL_USER
* splunk_credential_password -> SPLUNK_CREDENTIAL_PASSWORD

### TFS config env

* UserTfsConnectionAccount: -> TFS_ACCOUNT_USER
* UserTfsConnectionPassword: -> TFS_ACCOUNT_PASSWORD
* WindowsServiceLogonAccount: -> TFS_SERVICE_ACCOUNT_USER
* WindowsServiceLogonPassword: -> TFS_SERVICE_ACCOUNT_PASSWORD

### IIS website config env

* Credential_username -> WS_ANSIBLE_USER
* Credential_password -> WS_ANSIBLE_USER_PWD

### Log retention config env

* ExecuteAsCredential_username -> WS_ANSIBLE_USER
* ExecuteAsCredential_password -> WS_ANSIBLE_USER_PWD
